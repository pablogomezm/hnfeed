[![pipeline status](https://gitlab.com/kloness/hnfeed/badges/master/pipeline.svg)](https://gitlab.com/kloness/hnfeed/-/commits/master)
[![coverage report](https://gitlab.com/kloness/hnfeed/badges/master/coverage.svg)](https://gitlab.com/kloness/hnfeed/-/commits/master)

# HNFeed

Reign Full Stack Developer Test

## Installation and usage

You need to have [Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git) and [Docker](https://docs.docker.com/desktop/) installed.

Clone the repo

    git clone https://gitlab.com/kloness/hnfeed.git

Move to project directory

    cd hnfeed

Run docker-compose

    docker-compose up

Wait for it to finish, visit http://localhost and you will see the page

![page](https://gitlab.com/kloness/img/-/raw/master/page.png)

Clicking a row will open a link in a new tab.

Clicking the delete button will delete de news.

If you open Docker dashboard you will see 3 containers:
- backend: node image with the backend and API running, example route http://localhost:8080/api/news
- mongo: database container
- frontend_dev: react container, it runs a development version of react with hot reloading so you can make changes to the code and see them right away

![containers](https://gitlab.com/kloness/img/-/raw/master/dev.png)

To exit use ctrl + C.

### Production build of react

You can use the production version of react with the frontend_prod container. For this configuration use the `docker-compose.prod.yml` file:

    docker-compose --file docker-compose.prod.yml up

This separate docker-compose file will use a Docker multi-stage build to build the frontend project and move the build folder to be served with nginx.
You can visit http://localhost to see the page.
If you open Docker dashboard you will see the following containers

![containers](https://gitlab.com/kloness/img/-/raw/master/prod.png)
