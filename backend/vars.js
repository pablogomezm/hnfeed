
exports.MONGO_URL = process.env.MONGO_URL || 'mongodb://localhost:27017/hnfeed';
exports.MONGO_TEST_URL = process.env.MONGO_TEST_URL || 'mongodb://localhost:27017/test';
exports.PORT = process.env.PORT || 8080;
