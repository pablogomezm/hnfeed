// the Agenda library allows jobs to be run periodically
// https://github.com/agenda/agenda
const Agenda = require('agenda');
const vars = require('../vars');
const newsController = require('../controllers/news-controller');

const agenda = new Agenda({
  db: {
    address: vars.MONGO_URL,
    collection: 'jobs',
    options: {useUnifiedTopology: true}
  },
  processEvery: '1 seconds'
});

agenda.define('fetchAndSaveNewsJob', async () => {
  console.log('JOB: fetchAndSaveNews');
  await newsController.fetchAndSaveNews();
});

(async function() {
  await agenda.start();
  const executeAfterEveryRestart = false;
  if (executeAfterEveryRestart) {
    // after server restart it will trigger once and after that every hour
    await agenda.cancel({name: 'fetchAndSaveNewsJob'});
    await agenda.every('1 hour', 'fetchAndSaveNewsJob', {
      skipImmediate: false
    });
  } else {
    // it triggers every hour, it remembers the last time it ran after server restarts
    await agenda.every('1 hour', 'fetchAndSaveNewsJob');
  }
})();

module.exports = agenda;
