const express = require('express');

const newsController = require('../controllers/news-controller');

const router = express.Router();

// @desc  get a list of news
// @route /api/news
router.get('/', newsController.getNews);

// @desc  mark one news as deleted
// @route /api/news/:newsId
router.delete('/:newsId', newsController.deleteOneNews);

// @desc  remove existing news, fetch new ones and save them
// @route /api/news/fetch
router.get('/fetch', newsController.fetchNews);

module.exports = router;
