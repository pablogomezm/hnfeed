
const news1 = {
  "_id": "5ec9f10357f94c8f8c6d13a3",
  "title": "Deno JavaScript Runtime Engine",
  "author": "raviballa",
  "createdAt": "2020-05-22T18:56:28.000Z",
  "url": "https://www.infoworld.com/article/3518888/nodejs-rival-deno-emphasizes-security.html"
};


const newsModels = [
  news1,
  {
    "_id": "5ec9f10357f94c8f8c6d13a4",
    "title": "Route Between Nodes",
    "author": "srajaninnov",
    "createdAt": "2020-05-22T18:34:37.000Z",
    "url": "https://medium.com/@srajaninnov/route-between-nodes-a99d27498aed"
  },
  {
    "_id": "5ec9f10357f94c8f8c6d13a5",
    "title": "Most Detailed Node.js Tutorial for Beginners – Approx 5000 Words",
    "author": "codeforgeek",
    "createdAt": "2020-05-22T16:17:33.000Z",
    "url": "https://codeforgeek.com/node-js-tutorial-step-by-step/"
  }
]


const hit1 = {
  "created_at": "2020-05-22T18:56:28.000Z",
  "title": "Deno JavaScript Runtime Engine",
  "url": "https://www.infoworld.com/article/3518888/nodejs-rival-deno-emphasizes-security.html",
  "author": "raviballa",
  "story_title": null
}

const hit2 = {
  "created_at": "2020-05-22T18:34:37.000Z",
  "title": null,
  "url": "https://medium.com/@srajaninnov/route-between-nodes-a99d27498aed",
  "author": "srajaninnov",
  "story_title": "Route Between Nodes"
}

const hit3 = {
  "created_at": "2020-05-22T16:17:33.000Z",
  "title": null,
  "url": "https://codeforgeek.com/node-js-tutorial-step-by-step/",
  "author": "codeforgeek",
  "story_title": null
}

const angoliaResponse = {
  "data": {
    "hits": [hit1, hit2, hit3]
  }
}


exports.news1 = news1;
exports.newsModels = newsModels;

exports.hit1 = hit1;
exports.hit2 = hit2;
exports.hit3 = hit3;
exports.angoliaResponse = angoliaResponse;
