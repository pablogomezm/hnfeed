const request = require('supertest');
const expect = require('chai').expect;
const sinon = require('sinon');
const axios = require('axios');

const app = require('../app.js')
const News = require('../models/news-model');

const testData = require('./testData');


describe('routes test', () => {
  it('unsupported route should give a 404 response', async () => {
    const res = await request(app).get('/unknown_url');
    expect(res.status).to.equal(404);
  });
});


describe('get news API', () => {
  before(async () => {
    await News.deleteMany({});
    await News.insertMany(testData.newsModels);
  });
  it('get news API should return list of news', async () => {
    const res = await request(app).get('/api/news');
    expect(res.body).to.be.an('array');
    expect(res.body).to.deep.equal(testData.newsModels);
  });
  after(async () => {
    await News.deleteMany({});
  });
});


describe('delete news API', () => {
  before(async () => {
    await News.deleteMany({});
    await News.insertMany(testData.newsModels);
  });
  it('delete news API should mark the news as deleted', async () => {
    let news1 = await News.findById(testData.news1._id);
    expect(news1.deleted).to.be.false;
    const res = await request(app).delete(`/api/news/${testData.news1._id}`);
    expect(res.status).to.equal(200);
    news1 = await News.findById(testData.news1._id);
    expect(news1.deleted).to.be.true;
  });
  after(async () => {
    await News.deleteMany({});
  });
});


describe('fetch and save news API', () => {
  it('should fetch and save the news correctly', async () => {
    const axiosStub = sinon.stub(axios, 'get');
    axiosStub.resolves(Promise.resolve(testData.angoliaResponse));
    
    const res = await request(app).get('/api/news/fetch');
    const news = res.body;
    expect(news).to.be.an('array');

    // hit1 should be included as it is (it has title and url)
    expect(news).to.deep.include({
      title: testData.hit1.title,
      author: testData.hit1.author,
      createdAt: testData.hit1.created_at,
      url: testData.hit1.url
    });

    // hit2 should be included but the story title should be used instead of the title
    expect(news).to.deep.include({
      title: testData.hit2.story_title,
      author: testData.hit2.author,
      createdAt: testData.hit2.created_at,
      url: testData.hit2.url
    });

    // hit3 should not be included as it doesn't include title or story_title
    expect(news).to.not.deep.include({
      title: testData.hit3.title,
      author: testData.hit3.author,
      createdAt: testData.hit3.created_at,
      url: testData.hit3.url
    });
  });
});
