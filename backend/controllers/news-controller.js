const axios = require('axios');

const News = require('../models/news-model');


// remove existing news, fetch new ones and save them
// it is separate from the fetchNews endpoint so it can also be called from periodic jobs
const fetchAndSaveNews = async () => {
  // reset collection
  await News.deleteMany({});
  // fetch news
  const response = await axios.get('https://hn.algolia.com/api/v1/search_by_date?query=nodejs&tags=story');
  // get data from news
  const news = [];
  response.data.hits.map(hit => {
    // set title as story_title, if there is no story_title then use title
    const title = hit.story_title || hit.title;
    // if there is a title then add the news, if not then it is discarded
    if (title && hit.url) {
      news.push({
        title,
        author: hit.author,
        createdAt: hit.created_at,
        url: hit.url,
      })
    }
  });
  // insert news into the collection
  await News.insertMany(news);
  console.log(`${news.length} news fetched and saved`);
  return news;
}


const fetchNews = async (req, res) => {
  const news = await fetchAndSaveNews();
  res.json(news);
}


const getNews = async (req, res) => {
  // get news from database and return json
  const news = await News
    .find({ deleted: false })
    .select('title author createdAt url');
  res.json(news);
};


const deleteOneNews = async (req, res) => {
  News.findByIdAndUpdate(
    req.params.newsId,
    {deleted: true},
    (err) => {
      if (err) return res.status(500).send(err);
      return res.status(200).send('News deleted');
    }
  );
};


exports.fetchAndSaveNews = fetchAndSaveNews;
exports.fetchNews = fetchNews;
exports.getNews = getNews;
exports.deleteOneNews = deleteOneNews;
