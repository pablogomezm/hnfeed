const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const cors = require('cors');
require('dotenv').config();

const app = express();
const newsRoutes = require('./routes/news-routes');
const vars = require('./vars');

// if the app is beign called from the command line, this is true
// if the app is being imported from tests, this is false
const runningFromCommand = (require.main === module)

const MONGO_URL = runningFromCommand ? vars.MONGO_URL : vars.MONGO_TEST_URL;
const PORT = vars.PORT;

// setup CORS
app.use(cors());

// to be able to parse json and urlencoded data
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// routes
app.use('/api/news', newsRoutes);

app.use((req, res) => {
  // HTTP status 404: NotFound
  res.status(404).send('Not found');
});

mongoose.connect(
  MONGO_URL,
  {
    useNewUrlParser: true,
    useFindAndModify: false,
    useUnifiedTopology: true
  }
)
.then(() => {
  app.listen(PORT);
  if (runningFromCommand) require('./worker.js');  // schedule jobs
});

module.exports = app;
