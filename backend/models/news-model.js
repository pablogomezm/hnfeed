const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const newsSchema = new Schema({
    title: { type: String, required: true },
    author: { type: String, required: true },
    createdAt: { type: String, required: true },
    url: { type: String, required: true },
    deleted: { type: Boolean, required: false, default: false }
});

module.exports = mongoose.model('News', newsSchema);
