# build react app
FROM node:12-alpine as build_env

WORKDIR /usr/src/frontend

COPY package*.json ./

RUN npm ci

COPY . .

RUN npm run build

# production environment
FROM nginx:stable-alpine

COPY --from=build_env /usr/src/frontend/build /usr/share/nginx/html

EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]
