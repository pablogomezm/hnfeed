import React from 'react';
import { Typography, Container } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
 
const useStyles = makeStyles(theme => ({
  root: {
    background: '#2f2f2f',
    color: 'white',
    padding: '50px',
    margin: '0',
    fontWeight: 'bold',
  }
}));

export default function Hero() {
  const classes = useStyles();
 
  return (
    <Container classes={{ root: classes.root }} maxWidth={false}>
      <Typography variant='h2' gutterBottom>
        <b>HN Feed</b>
      </Typography>

      <Typography gutterBottom>
        We {'<'}3 hacker news!
      </Typography>
    </Container>
  );
}
