import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';

import { getNews, deleteNews } from '../api/news';
import convertDates from '../util/convertDates';

const useStylesTable = makeStyles({
  table: {
    minWidth: 650
  }
});

const useStylesAuthor = makeStyles({
  root: {
    marginLeft: '10px'
  }
});

const useStylesTableRow = makeStyles({
  root: {
    cursor: 'pointer'
  }
});

const NewsTable = (props) => {
  const tableClasses = useStylesTable();
  const authorClasses = useStylesAuthor();
  const tableRowClasses = useStylesTableRow();

  // UI: Title - Author -
  const titleCell = (row) => (
    <TableCell component='th' scope='row'>
      {row.title}
      <Typography
        variant='caption'
        display='inline'
        color='textSecondary'
        className={authorClasses.root}
      >
        - {row.author} -
      </Typography>
    </TableCell>
  )

  return (
    <TableContainer component={Paper}>
      <Table className={tableClasses.table} aria-label='news table'>
        <TableBody>
          {props.news.map((row) => (
            <TableRow
              key={row._id} hover
              onClick={props.handleRowClick(row.url)}
              className={tableRowClasses.root}
            >
              {/* Title - Author - */}
              {titleCell(row)}

              {/* Yesterday */}
              <TableCell align='right'>{row.createdAt}</TableCell>

              {/* Delete button */}
              <TableCell align='right'>
                <IconButton onClick={props.handleDeleteNews(row._id)} aria-label="delete" size='small'>
                  <DeleteIcon />
                </IconButton>
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}


// NewsSection: state and logic
// NewsTable: presentation
export default function NewsSection() {
  const [news, setNews] = useState([]);

  useEffect(() => {
    // fetch news and save them in state
    // this function is only executed at the start
    async function loadNews() {
      let newsData = await getNews();
      newsData = convertDates(newsData);  // humanize dates
      setNews(newsData);
    }
    loadNews();
  }, []);

  const handleDeleteNews = (newsId) => {
    // this function doesn't remove the news immediately,
    // it gives a function ready to be called when necessary
    const removeNews = async (event) => {
      event.stopPropagation();  // to prevent triggering TableRow onClick
      const wasDeleted = await deleteNews(newsId);
      // if it was deleted on backend, also delete it from the state
      if (wasDeleted) setNews(news.filter(item => item._id !== newsId));
    }
    return removeNews;
  }

  const handleRowClick = (url) => {
    // this function doesn't open the link immediately,
    // it gives a function ready to be called when necessary
    const openLink = () => {
      window.open(url, '_blank');
    }
    return openLink;
  }

  return (
    <NewsTable
      news={news}
      handleDeleteNews={handleDeleteNews}
      handleRowClick={handleRowClick}
    />
  );
}
