import moment from 'moment';

const humanizeDate = (date) => {
  // example: "2020-05-21T00:44:03.000Z" -> "5 hours ago"
  const dif = moment(date).diff(moment(), 'minutes');  // minutes between the date and now
  return moment.duration(dif, 'minutes').humanize(true)
}

const convertDates = (news) => {
  // take an array of news, for each news replace the date with a humanized text
  /* example:
  {
    "title": "title",
    "author": "author",
    "createdAt": "2020-05-21T00:44:03.000Z"
  }
  ->
  {
    "title": "title",
    "author": "author",
    "createdAt": "5 hours ago"
  }
  */
 return news.map((aNews) => ({
   ...aNews,
   createdAt: humanizeDate(aNews.createdAt)
 }))
}

export default convertDates;
