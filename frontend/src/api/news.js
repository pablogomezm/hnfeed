import axios from 'axios';

const backendBaseUrl = 'http://localhost:8080';

export const getNews = async () => {
  /* It fetches a list of news. Response example:
  [
    {
    "_id": "5ec6fee46253eb2aae7d9726",
    "title": "Fake emails spreads new Node.js malware",
    "author": "rmason",
    "createdAt": "2020-05-21T20:25:23.000Z",
    "url": "https://www.bleepingcomputer.com/news/security/fake-us-dept"
    }
  ]
  */
  const res = await axios.get(`${backendBaseUrl}/api/news`);
  return res.data
}

export const deleteNews = async (newsId) => {
  // calls delete news API, if the call is successful return true, if not return false
  return axios.delete(`${backendBaseUrl}/api/news/${newsId}`)
    .then(() => true)
    .catch(() => false);
}
