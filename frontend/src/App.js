import React from 'react';
import { Container } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

import Hero from './components/Hero';
import NewsSection from './components/NewsSection';

// styles for Container
const useStylesContainer = makeStyles({
  root: {
    marginTop: '1rem',
    marginBottom: '2rem',
  },
});

const App = () => {
  const containerClasses = useStylesContainer();

  return (
    <>
      <Hero />
      <Container className={containerClasses.root} maxWidth='md'>
        <NewsSection />
      </Container>
    </>
  )
}

export default App;
